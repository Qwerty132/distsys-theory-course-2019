
- [Building Large-Scale Internet Services @ Google](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/44877.pdf)

---

- [Linkedin Ambry](https://github.com/linkedin/ambry/wiki)
- [Apache Kafka Design](https://kafka.apache.org/documentation/#design)
- [LogDevice: a distributed data store for logs](https://code.fb.com/core-data/logdevice-a-distributed-data-store-for-logs/)
- [CocroachDB Architecture](https://www.cockroachlabs.com/docs/stable/architecture/overview.html)
- [TiKV Deep Dive](https://tikv.org/deep-dive/)
- [Consul Architecture](https://www.consul.io/docs/internals/architecture.html)
- [FaunaDB Architectural Overview](https://fauna-assets.s3.amazonaws.com/public/FaunaDB-Technical-Whitepaper.pdf)